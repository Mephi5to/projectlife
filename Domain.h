#ifndef LIFE_DOMAIN_H
#define LIFE_DOMAIN_H

#include <fstream>
#include <string>
#include <vector>
#include <memory>

using std::string;
using std::vector;

namespace Domain {

    class Field {
    public:
        Field(int x = 10, int y = 10);
        Field(const Field &other);
        ~Field() = default;

        Field &operator=(const Field &other);

        void setAliveCell(int x, int y);
        void killCell(int x, int y);
        void reset();

        char getCell(int x, int y) const;
        int getHeight() const;
        int getWidth() const;

    private:
        vector<vector<char>> field;
    };

    struct Rules {
        explicit Rules(int m = 3, int n = 2, int k = 3);
        Rules(const Rules &other);

        int m;
        int n;
        int k;
    };

    struct Configuration {
        Configuration(Field &field, Rules &rules, const string &fname, int countIters = 10);
        Configuration(const Configuration &other);

        Field field;
        string fileOutName;
        int countIters;
        Rules rules;
    };

    struct Data {
        Data(Field &field, Rules &rules);

        Field &field;
        Rules rules;
    };
}

#endif //LIFE_DOMAIN_H
