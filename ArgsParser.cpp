#include "ArgsParser.h"
#include <iostream>

namespace ArgsParserSpace {
    int FIRST_ARG = 0;
    int SECOND_ARG = 1;
    int THIRD_ARG = 2;
    int DEFAULT_ITERS = 10;
    int DEFAULT_WIDTH = 10;
    int DEFAULT_HEIGHT = 10;

    using std::make_shared;

    bool isOnlyCommand(string str) {
        return str == "field" || str == "rules";
    }

    vector<Option> setOptions() {
        vector<Option> opts = {
                Option({make_shared<LongForm>("RULES")}, {make_shared<IntArg>("m", "<int>"), make_shared<IntArg>("n", "<int>"), make_shared<IntArg>("k", "<int>")}, "rules",
                       "rules for game Life"),
                Option({make_shared<LongForm>("FIELD")}, {make_shared<IntArg>("<x>", "<int>"), make_shared<IntArg>("<y>", "<int>")},
                       "field", "size of field"),
                Option({make_shared<LongForm>("SET")}, {make_shared<IntArg>("<x>", "<int>"), make_shared<IntArg>("<y>", "<int>")}, "set",
                       "setAliveCell")
        };

        return opts;
    }

    vector<ActualOption> isCorrectFile(vector<string> &args) {
        RuleSet::OptionRuleSet ruleSet(setOptions());
        vector<ActualOption> opts;

        try {
            opts = ruleSet.parse(args);
        }
        catch (RuleSet::ExactMismatch) {
            std::cerr << "Undefined command in file";
            exit(EXIT_FAILURE);

        }
        catch (RuleSet::PartialMatch &err) {
            std::cerr << "Bad arguments in file" << "\n";
            exit(EXIT_FAILURE);
        }
        catch (RuleSet::NegativeArgument) {
            std::cerr << "Expected negative argument" << "\n";
            exit(EXIT_FAILURE);
        }

        for (int i = 0; i < opts.size() - 1; ++i) {
            for (int j = i + 1; j < opts.size(); ++ j) {
                if (isOnlyCommand(opts[i].getOpt().getName()) && (opts[i].getOpt().getName() == opts[j].getOpt().getName())) {
                    std::cerr << "Too much command: " << opts[i].getOpt().getName();
                    exit(EXIT_FAILURE);
                }
            }
        }

        return opts;
    }

    vector<ActualOption> parseFile(std::ifstream &stream) {
        string line;
        string command;
        vector<string> args;

        char smbl;
        string arg;
        while (!stream.eof()) {
            smbl = static_cast<char>(stream.peek());
            if (smbl == '#') {
                std::getline(stream, line);
                continue;
            }
            else if (smbl == ' ' || smbl == '\n') {
                stream.get();
                smbl = static_cast<char>(stream.peek());
                continue;
            }
            else {
                stream >> command;
                args.emplace_back(command);
            }
        }

        vector<ActualOption> options = isCorrectFile(args);

        return options;
    }

    void setFileOptions(const vector<ActualOption> &fileOptions, int &height, int &width, Rules &rules) {
        for (int i = 0; i < fileOptions.size(); ++i) {
            if (fileOptions[i].getOpt().getName() == "field") {
                width = stoi(fileOptions[i].getArgs()[FIRST_ARG]);
                height = stoi(fileOptions[i].getArgs()[SECOND_ARG]);
            }

            if (fileOptions[i].getOpt().getName() == "rules") {
                rules.m = stoi(fileOptions[i].getArgs()[FIRST_ARG]);
                rules.n = stoi(fileOptions[i].getArgs()[SECOND_ARG]);
                rules.k = stoi(fileOptions[i].getArgs()[THIRD_ARG]);
            }
        }
    }

    void setCmdOptions(const vector<ActualOption> &cmdOpts, string &fileOutName, int &iters, int &height, int &width, Rules &rules) {
        for (const auto &opt : cmdOpts) {
            if (opt.getOpt().getName() == "iteration") {
                iters = std::stoi(opt.getArgs()[FIRST_ARG]);
            }
            if (opt.getOpt().getName() == "field") {
                width = std::stoi(opt.getArgs()[FIRST_ARG]);
                height = std::stoi(opt.getArgs()[SECOND_ARG]);
            }
            if (opt.getOpt().getName() == "output") {
                fileOutName = opt.getArgs()[FIRST_ARG];
            }
            if (opt.getOpt().getName() == "M") {
                rules.m = std::stoi(opt.getArgs()[FIRST_ARG]);
            }
            if (opt.getOpt().getName() == "N") {
                rules.n = std::stoi(opt.getArgs()[FIRST_ARG]);
            }
            if (opt.getOpt().getName() == "K") {
                rules.k = std::stoi(opt.getArgs()[FIRST_ARG]);
            }
        }
    }

    void setField(Field &field, vector<ActualOption> &fileOpts) {
        for (auto &fileOpt : fileOpts) {
            if (fileOpt.getOpt().getName() == "set") {
                field.setAliveCell(stoi(fileOpt.getArgs()[FIRST_ARG]), stoi(fileOpt.getArgs()[SECOND_ARG]));
            }
        }
    }

    Configuration SetConfig(const vector<RuleSet::ActualOption> &cmdOpts) {
        int iters = DEFAULT_ITERS;
        int height = DEFAULT_HEIGHT;
        int width = DEFAULT_WIDTH;
        Rules rules;
        string fileInName;
        string fileOutName = "output.txt";
        vector<ActualOption> fileOptions;

        for (const auto &opt : cmdOpts) {
            if (opt.getOpt().getName() == "input") {
                fileInName = opt.getArgs()[FIRST_ARG];
            }
        }
        std::ifstream fileIn(fileInName);
        if (!fileIn) {
            std::cerr << "No input file" << "\n";
            exit(EXIT_FAILURE);
        }
        fileOptions = parseFile(fileIn);


        setFileOptions(fileOptions, height, width, rules);
        setCmdOptions(cmdOpts, fileOutName, iters, height, width, rules);

        Field field(width, height);
        setField(field, fileOptions);

        Configuration config(field, rules, fileOutName, iters);

        return config;

    }
}