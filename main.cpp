#include <fstream>
#include "Parser.h"
#include "Driver.h"
#include "ArgsParser.h"

int main(int argc, char* argv[]) {
    vector<string> data;

    for (int i = 1; i < argc; ++i) {
        data.emplace_back(argv[i]);
    }
    if (argc > 1) {
        vector<RuleSet::ActualOption> options;
        RuleSet::OptionRuleSet ruleSet(CmdParser::setOptions());
        options = CmdParser::parseCmd(ruleSet, data);
        Domain::Configuration config(ArgsParserSpace::SetConfig(options));
        Driver::AutoMode game(config.field, config.fileOutName, config. rules);
        game.Life(config.countIters);
        game.saveToFile();
    }
    else {
        Driver::InteractiveMode game;
        game.life();
    }



    return EXIT_SUCCESS;
}