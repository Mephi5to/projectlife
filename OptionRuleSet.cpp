#include "OptionRuleSet.h"
#include <iostream>


namespace RuleSet {
    int ONLY_NAME = 1;

    bool isVarCountOfArgs(const string &name) {
        return name == "step";
    }

    vector<ActualOption> OptionRuleSet::parse(vector<string> &str) {
        vector<ActualOption> actualOpt;
        auto enditer = str.begin();
        auto iterEnd = str.end();
        auto iter = str.begin();

        while(iter != iterEnd) {
            bool err = true;

            for (auto &opt : opts) {
                if (opt.isValid(*iter)) {
                    if (isVarCountOfArgs(opt.getName()) && str.size() == ONLY_NAME) {
                        ActualOption option(opt, {"1"});
                        actualOpt.push_back(option);
                        iter = iterEnd;
                        err = false;
                        break;
                    }
                    enditer += opt.getCountArgs() + 1;
                    actualOpt.push_back(opt.parse(iter + 1, enditer));
                    iter = enditer;
                    err = false;

                }
            }

            if (err) {
                throw (ExactMismatch());
            }

        }

        return actualOpt;
    }

    ActualOption Option::parse(vector<string>::iterator begin, vector<string>::iterator end) {
        vector<string> cmdArgs;
        vector<string>::iterator iter;
        int i = 0;

        for (iter = begin; iter < end; ++iter) {
            cmdArgs.push_back(*iter);
        }
        ActualOption actualOpt(*this, cmdArgs);

        for (iter = begin; iter < end; ++iter) {
            if (!getArgs()[i]->isValid(*iter)) {
                throw (PartialMatch(this));
            }
            i++;
        }

        return actualOpt;
    }

    string OptionRuleSet::getHelp() {
        for (auto &opt : opts) {
            std::cout << "Description: " << opt.getDescription() << ", ";
            std::cout << "Name: " << opt.getName() << ", ";

            std::cout << "Forms: ";
            for (int j = 0; j < opt.getForms().size(); ++j) {
                std::cout << opt.getForms()[j]->getHint() << ", ";
            }

            std::cout << "list of arguments: ";
            for (int k = 0; k < opt.getArgs().size(); ++k) {
                std::cout << opt.getArgs()[k]->getDescription() << " ";
                std::cout << opt.getArgs()[k]->getHint() << " ";
            }

            std::cout << "\n";
        }

    }

    OptionRuleSet::OptionRuleSet(const vector<Option> &options) : opts(options) {

    }

    Option::Option(const vector<shared_ptr<OptionForm>> &frms, const vector<shared_ptr<OptionArg>> &arguments, const string &name, const string &description)
            : forms(frms), args(arguments), name(name), description(description)
    {

    }

    string Option::getDescription()  const {
        return description;
    }

    int Option::getCountArgs()  const {
        return args.size();
    }

    string Option::getName() const {
        return name;
    }

    vector<shared_ptr<OptionForm>> Option::getForms() {
        return forms;
    }

    vector<shared_ptr<OptionArg>> Option::getArgs() {
        return args;
    }

    bool Option::isValid(string &str) {
        for (auto &form : forms) {
            if (form->isValid(str)) {
                return  true;
            }
        }

        return false;
    }

    bool IntArg::isValid(string &str) {
        try {
            std::stoi(str);
        }
        catch (...) {
            return false;
        }

        int number = std::stoi(str);
        if (number < 0) {
            throw(NegativeArgument());
        }

        return true;
    }

    string IntArg::getDescription() {
        return description;
    }

    IntArg::IntArg(const string &description, const string &hint)
            : description(description), hint(hint)
    {

    }

    string IntArg::getHint() {
        return hint;
    }

    bool StrArg::isValid(string &str) {
        return true;
    }

    string StrArg::getDescription() {
        return description;
    }

    StrArg::StrArg(const string &description, const string &hint)
        : description(description), hint(hint)
    {

    }

    string StrArg::getHint() {
        return hint;
    }

    LongForm::LongForm(const string &frm) : form(frm) {}

    bool LongForm::isValid(string &str) {
        return (str == form);
    }

    string LongForm::getHint() const {
        return form;
    }

    ShortForm::ShortForm(const string &frm) : form(frm) {}

    bool ShortForm::isValid(string &str) {
        return (str == form);
    }

    string ShortForm::getHint() const {
        return form;
    }

    ActualOption::ActualOption(const Option &option, const vector<string> &arguments) : args(arguments){
        opt = std::make_shared<Option>(option);
    }

    vector<string> ActualOption::getArgs() const {
        return args;
    }

    Option ActualOption::getOpt() const {
        return *opt;
    }

    Option PartialMatch::getOpt() {
        return *opt;
    }

    PartialMatch::PartialMatch(const Option *option) {
        opt = std::make_shared<Option>(*option);
    }
}