#include "Domain.h"
#include "Output.h"

namespace Domain {

    Configuration::Configuration(Field &field, Rules &rules, const string &fname, int countIters)
            : field(field), rules(rules), fileOutName(fname), countIters(countIters)
    {

    }

    Configuration::Configuration(const Configuration &other) {
        field = other.field;
        rules = other.rules;
        fileOutName = other.fileOutName;
        countIters = other.countIters;

    }

    Field::Field(int x, int y) {
        field.resize(x);
        for (int i = 0; i < x; ++i) {
            for (int j = 0; j < y; ++j) {
                field[i].push_back('.');
            }
        }
    }

    void Field::setAliveCell(int x, int y) {
        field[x][y] = '*';
    }



    void Field::killCell(int x, int y) {
        field[x][y] = '.';
    }

    Field::Field(const Field &other) {
        field.resize(other.field.size());
        for (int i = 0; i < other.field.size(); ++i) {
            for (int j = 0; j < other.field[0].size(); ++j) {
                field[i].push_back(other.field[i][j]);
            }
        }
    }

    Field &Field::operator=(const Field &other) {
        field.resize(other.field.size());
        for (int i = 0; i < other.field.size(); ++i) {
            field[i].resize(other.field[i].size());
        }
        for (int i = 0; i < other.field.size(); ++i) {
            for (int j = 0; j < other.field[0].size(); ++j) {
                field[i][j] = other.field[i][j];
            }
        }

        return *this;
    }

    int Field::getHeight() const {
        return field[0].size();
    }

    int Field::getWidth() const {
        return field.size();
    }

    char Field::getCell(int x, int y) const {
        return field[x][y];
    }

    void Field::reset() {
        for (auto &i : field) {
            for (int j = 0; j < field[0].size(); ++j) {
                i[j] = '.';
            }
        }
    }

    Data::Data(Field &field, Rules &rules) : field(field), rules(rules) {

    }


    Rules::Rules(int m, int n, int k) : m(m), n(n), k(k) {

    }

    Rules::Rules(const Rules &other) {
        m = other.m;
        n = other.n;
        k = other.k;
    }

};