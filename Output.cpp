#include "Output.h"


namespace OutPut {

    Writer::Writer(std::ostream &stream) : out(stream) {
    }

    void Writer::write(Data &data) {
        out << "FIELD " << data.field.getWidth() << " " << data.field.getHeight() << "\n";
        out << "RULES " << data.rules.m << " " << data.rules.n << " " << data.rules.k << "\n";
        for (int i = 0; i < data.field.getWidth(); ++i) {
            for (int j = 0; j < data.field.getHeight(); ++j) {
                if (data.field.getCell(i, j) == '*') {
                    out << "SET " << i << " " << j << "\n";
                }
            }
        }
    }
}
