#ifndef LIFE_OUTPUT_H
#define LIFE_OUTPUT_H

#include <vector>
#include <string>
#include <fstream>

#include "Domain.h"

namespace OutPut {
    using namespace Domain;

    class Writer {
    public:
        Writer(std::ostream &stream);

        void write(Data &data);

    private:
        std::ostream &out;
    };


}

#endif //LIFE_OUTPUT_H


