#ifndef LIFE_PARSER_H
#define LIFE_PARSER_H

#include "OptionRuleSet.h"



namespace CmdParser {
    using namespace RuleSet;

    vector<ActualOption> parseCmd(OptionRuleSet &ruleSet, vector<string> &data);

    vector<Option> setOptions();

}

#endif //LIFE_PARSER_H
