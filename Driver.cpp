#include "Driver.h"
#include "ArgsParser.h"
#include "Output.h"
#include "Parser.h"
#include <iostream>
#include <cmath>

namespace Driver {
    int FIRST_ARG = 0;
    int SECOND_ARG = 1;
    int THIRD_ARG = 2;
    int DEFAULT_WIDTH = 10;
    int DEFAULT_HEIGHT = 10;

    using std::make_shared;
    vector<std::shared_ptr<InteractiveMode>> InteractiveMode::stackStates;

    vector<Option> InteractiveMode::setOptions() {
        vector<Option> opts = {
                Option({make_shared<LongForm>("reset")}, {}, "reset",
                       "reset field and stepsCounter"),
                Option({make_shared<LongForm>("set")}, {make_shared<IntArg>("<x>", "<int>"), make_shared<IntArg>("<y>", "<int>")},
                       "set", "set alive cell"),
                Option({make_shared<LongForm>("clear")}, {make_shared<IntArg>("<x>", "<int>"), make_shared<IntArg>("<y>", "<int>")},
                       "clear", "kill alive cell"),
                Option({make_shared<LongForm>("load")}, {make_shared<StrArg>("<input file>", "<filename>")}, "load",
                       "save game to file"),
                Option({make_shared<LongForm>("step")}, {make_shared<IntArg>("<n>", "<int>")},
                       "step", "go to n-th step"),
                Option({make_shared<LongForm>("back")}, {},
                       "back", "go back one step"),
                Option({make_shared<LongForm>("save")}, {make_shared<StrArg>("<output file>", "<filename>")}, "save",
                       "load game from file"),
                Option({make_shared<LongForm>("rules")}, {make_shared<IntArg>("<m>", "<int>"), make_shared<IntArg>("<n>", "<int>"), make_shared<IntArg>("<k>", "<int>")},
                       "rules", "set rules"),
                Option({make_shared<LongForm>("help")}, {}, "help", "description every command")
        };

        return opts;
    }

    int countAliveNeighbours(int x, int y, Field &field) {
        int w = field.getHeight();
        int h = field.getWidth();
        int m;
        int k;
        int countAlive = 0;

        for (int i = x - 1; i < x + 2; ++i) {
            for (int j = y - 1; j < y + 2; ++j) {
                if (i == x && j == y) {
                    continue;
                }
                m = i < 0 || i == h ? h - abs(i) : i;
                k = j < 0 || j == w ? w - abs(j) : j;
                if (field.getCell(m, k) == '*') {
                    countAlive++;
                }
            }
        }

        return countAlive;
    }

    void neighboursStrategy(Rules &rules, Field &field, Field &nextField) {
        int count;

        for (int i = 0; i < field.getWidth(); ++i) {
            for (int j = 0; j < field.getHeight(); ++j) {
                count = countAliveNeighbours(i, j, field);
                if (field.getCell(i, j) == '.' && count == rules.m) {
                    nextField.setAliveCell(i, j);
                }
                if (field.getCell(i, j) == '*' && count < rules.n) {
                    nextField.killCell(i, j);
                }
                if (field.getCell(i, j) == '*' && count > rules.k) {
                    nextField.killCell(i, j);
                }
            }
        }
    }

    AutoMode::AutoMode(Field &field, const string &fname, Rules &rules)
            : field(field), outFile(fname), rules(rules)
    {

    }

    void AutoMode::saveToFile() {
        Data outData(field, rules);
        OutPut::Writer writer(outFile);

        writer.write(outData);
    }

    void AutoMode::Life(const int &iters) {
        Field nextField(field);

        for (int i = 0; i < iters; ++i) {
            neighboursStrategy(rules, field, nextField);
            field = nextField;
        }

    }

    InteractiveMode::InteractiveMode(Rules &rules, int x, int y, int steps)
            : rules(rules), field(x, y), stepsCounter(steps)
    {

    }

    void InteractiveMode::changeRules(Rules &rls) {
        rules = rls;
    }

    void InteractiveMode::clear(int x, int y) {
        field.killCell(x, y);
    }

    void InteractiveMode::set(int x, int y) {
        field.setAliveCell(x, y);
    }

    void InteractiveMode::reset() {
        field.reset();
        stepsCounter = 0;
    }

    void InteractiveMode::step(int n) {
        Field nextField(field);

        for (int i = 0; i < n; ++i) {
            neighboursStrategy(rules, field, nextField);
            field = nextField;
        }
    }

    vector<string> readCommand() {
        vector<string> args;
        string line;
        string arg;

        getline(std::cin, line);
        for (char i : line) {
            if (i == ' ') {
                if (!arg.empty()) {
                    args.push_back(arg);
                    arg = "";
                }
                continue;
            }
            else {
                arg += i;
            }
        }
        if (line[line.size() - 1] != ' ') {
            args.push_back(arg);
        }

        return args;
    }

    void InteractiveMode::doCommand(vector<ActualOption> &options) {
        auto state = std::make_shared<InteractiveMode>(*this);
        stackStates.emplace_back(state);
        for (auto &opt : options) {
            if (opt.getOpt().getName() == "load") {
                string fname = opt.getArgs()[FIRST_ARG];
                load(fname);
                stepsCounter++;
            }
            if (opt.getOpt().getName() == "save") {
                save(opt.getArgs()[FIRST_ARG]);
                stepsCounter++;
            }
            if (opt.getOpt().getName() == "reset") {
                reset();
            }
            if (opt.getOpt().getName() == "set") {
                set(stoi(opt.getArgs()[FIRST_ARG]), stoi(opt.getArgs()[SECOND_ARG]));
                stepsCounter++;
            }
            if (opt.getOpt().getName() == "clear") {
                clear(stoi(opt.getArgs()[FIRST_ARG]), stoi(opt.getArgs()[SECOND_ARG]));
                stepsCounter++;
            }
            if (opt.getOpt().getName() == "step") {
                step(stoi(opt.getArgs()[FIRST_ARG]));
                stepsCounter++;
            }
            if (opt.getOpt().getName() == "rules") {
                Rules rules(stoi(opt.getArgs()[FIRST_ARG]), stoi(opt.getArgs()[SECOND_ARG]), stoi(opt.getArgs()[THIRD_ARG]));
                changeRules(rules);
                stepsCounter++;
            }
            if (opt.getOpt().getName() == "back") {
                stackStates.pop_back();
                back();
            }
        }
    }

    void InteractiveMode::life() {
        RuleSet::OptionRuleSet ruleSet(setOptions());
        vector<ActualOption> options;
        vector<string> cmdData;
        string command;

        while(1) {
            cmdData = readCommand();
            if (cmdData.empty()) {
                std::cout << "No such command" << "\n";
                continue;
            }
            options = CmdParser::parseCmd(ruleSet, cmdData);
            doCommand(options);
            printCmd();

        }

    }

    void InteractiveMode::back() {
        if (stackStates.empty()) {
            std::cerr << "Stack of states is empty!";
            exit(EXIT_FAILURE);
        }

        InteractiveMode previous(*stackStates.back());
        stackStates.pop_back();
        field = previous.field;
        rules = previous.rules;
        stepsCounter = previous.stepsCounter;

    }


    void InteractiveMode::save(const string &fname) {
        std::ofstream out(fname);
        OutPut::Writer writer(out);
        Data outData(field, rules);

        writer.write(outData);
    }

    void InteractiveMode::load(const string &fname) {
        int height = DEFAULT_HEIGHT;
        int width = DEFAULT_WIDTH;
        Rules rules;
        vector<ActualOption> fileOptions;

        std::ifstream fileIn(fname);
        if (!fileIn) {
            std::cerr << "No such input file" << "\n";
            exit(EXIT_FAILURE);
        }
        fileOptions = ArgsParserSpace::parseFile(fileIn);

        ArgsParserSpace::setFileOptions(fileOptions, height, width, rules);
        Field newField(width, height);
        ArgsParserSpace::setField(newField, fileOptions);
        field = newField;
    }

    InteractiveMode::InteractiveMode(InteractiveMode &other) {
        field = other.field;
        stepsCounter = other.stepsCounter;
        rules = other.rules;
    }

    void InteractiveMode::printCmd() {
        std::cout << "Step: " << stepsCounter << "\n\n";

        for (int i = 0; i < field.getWidth(); ++i) {
            for (int j = 0; j < field.getHeight(); ++j) {
                std::cout << field.getCell(i, j);
            }
            std::cout << "\n";
        }
    }

    InteractiveMode::InteractiveMode() : field(), rules() {
        stepsCounter = 0;
    }

};