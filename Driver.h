#ifndef LIFE_DRIVER_H
#define LIFE_DRIVER_H

#include <fstream>
#include <string>
#include <vector>
#include <stack>

#include "OptionRuleSet.h"
#include "Domain.h"

namespace Driver {
    using namespace Domain;
    using RuleSet::Option;
    using RuleSet::LongForm;
    using RuleSet::IntArg;
    using RuleSet::ActualOption;
    using RuleSet::StrArg;
    class InteractiveMode;

    int countAliveNeighbours(int x, int y, Field &field);

    void neighboursStrategy(Rules &rules, Field &field, Field &nextField);

    class InteractiveMode {
    public:
        explicit InteractiveMode(Rules &rules, int x = 10, int y = 10, int steps = 0);
        InteractiveMode(InteractiveMode &other);
        InteractiveMode();

        vector<Option> setOptions();
        void life();


    private:
        static vector<std::shared_ptr<InteractiveMode>> stackStates;
        void doCommand(vector<ActualOption> &options);
        void printCmd();

        void load(const string &fname);
        void save(const string &fname);
        void reset();
        void set(int x, int y);
        void clear(int x, int y);
        void step(int n = 1);
        void back();
        void changeRules(Rules &rls);

        Field field;
        int stepsCounter;
        Rules rules;


    };

    class AutoMode {
    public:
        AutoMode(Field &field, const string &fname, Rules &rules);
        ~AutoMode() = default;

        void Life(const int &iters);
        void saveToFile();

    private:
        Field &field;
        Rules rules;
        std::ofstream outFile;

    };

}

#endif //LIFE_DRIVER_H
