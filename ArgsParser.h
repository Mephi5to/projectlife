#ifndef LIFE_ARGSPARSER_H
#define LIFE_ARGSPARSER_H

#include "OptionRuleSet.h"
#include "Domain.h"
#include <fstream>


namespace ArgsParserSpace {
    using Domain::Field;
    using Domain::Rules;
    using RuleSet::Option;
    using RuleSet::LongForm;
    using RuleSet::IntArg;
    using RuleSet::ActualOption;
    using Domain::Configuration;

    void setField(Field &field, vector<ActualOption> &fileOpts);

    void setFileOptions(const vector<ActualOption> &fileOptions, int &height, int &width, Rules &rules);

    void setCmdOptions(const vector<ActualOption> &cmdOpts, string &fileOutName, int &iters, int &height, int &width, Rules &rules);

    vector<ActualOption> isCorrectFile(vector<string> &args);

    vector<Option> setOptions();

    vector<ActualOption> parseFile(std::ifstream &stream);

    Configuration SetConfig(const vector<RuleSet::ActualOption> &opts);

}

#endif //LIFE_ARGSPARSER_H
