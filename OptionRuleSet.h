#ifndef LIFE_OPTIONRULESET_H
#define LIFE_OPTIONRULESET_H

#include <fstream>
#include <string>
#include <vector>
#include <memory>

using std::vector;
using std::string;
using std::shared_ptr;

namespace RuleSet {

    class OptionForm;
    class OptionArg;
    class ExactMismatch;
    class ActualOption;
    class OptionForm;
    class OptionArg;
    class Option;

    class OptionRuleSet {
    public:
        explicit OptionRuleSet(const vector<Option> &options);
        string getHelp();
        vector<ActualOption> parse(vector<string> &str);

    private:
        vector<Option> opts;
    };

    class Option {
    public:
        Option(const vector<shared_ptr<OptionForm>> &forms, const vector<shared_ptr<OptionArg>> &args, const string &name, const string &description);
        virtual ~Option() = default;;

        ActualOption parse(vector<string>::iterator begin, vector<string>::iterator end);
        bool isValid(string &str);

        int getCountArgs() const;
        string getName() const;
        string getDescription() const;
        vector<shared_ptr<OptionForm>> getForms();
        vector<shared_ptr<OptionArg>> getArgs();

    private:
        vector<shared_ptr<OptionForm>> forms;
        vector<shared_ptr<OptionArg>> args;
        string name;
        string description;
    };

    class OptionForm {
    public:
        virtual bool isValid(string &str) = 0;
        virtual string getHint() const = 0;
        virtual ~OptionForm() {}

    };

    class LongForm : public OptionForm {
    public:
        LongForm(const string &frm);
        string getHint() const override;
        bool isValid(string &str) override;

    private:
        string form;
    };

    class ShortForm : public OptionForm {
    public:
        ShortForm(const string &frm);
        string getHint() const override;
        bool isValid(string &str) override;

    private:
        string form;
    };

    class OptionArg {
    public:
        virtual  bool isValid(string&) = 0;
        virtual string getHint() = 0;
        virtual string getDescription() = 0;
        virtual ~OptionArg() {}

    };

    class IntArg : public OptionArg {
    public:
        IntArg(const string &description, const string &hint);
        string getHint()  override;
        string getDescription() override;
        bool isValid(string &str) override;

    private:
        string description;
        string hint;
    };

    class StrArg : public OptionArg {
    public:
        StrArg(const string &description, const string &hint);
        bool isValid(string &str) override;
        string getHint()  override;
        string getDescription() override;

    private:
        string description;
        string hint;
    };

    class ActualOption {
    public:
        ActualOption(const Option &option, const vector<string> &arguments);

        vector<string> getArgs() const;
        Option getOpt() const;

    private:
        shared_ptr<Option> opt;
        vector<string> args;

    };

    class ExactMismatch {

    };

    class NegativeArgument {

    };

    class PartialMatch {
    public:
        explicit PartialMatch(const Option *option);
        Option getOpt();

    private:
        shared_ptr<Option> opt;
    };

}

#endif //LIFE_OPTIONRULESET_H
