#include "Parser.h"
#include <iostream>


namespace CmdParser {
    using std::make_shared;

    vector<Option> setOptions() {
        vector<Option> opts = {
                Option({make_shared<ShortForm>("-ic"), make_shared<LongForm>("--iterations")}, {make_shared<IntArg>("<iteration count>", "<int>")}, "iteration",
                       "generations in game Life"),
                Option({make_shared<ShortForm>("-f"), make_shared<LongForm>("--field")}, {make_shared<IntArg>("<x>", "<int>"), make_shared<IntArg>("<y>", "<int>")},
                       "field", "size of field"),
                Option({make_shared<ShortForm>("-if"), make_shared<LongForm>("--input")}, {make_shared<StrArg>("<input file>", "<*.lf>")}, "input",
                       "input file"),
                Option({make_shared<ShortForm>("-o"), make_shared<LongForm>("--output")}, {make_shared<StrArg>("<output file>", "<*.lf>")}, "output",
                       "output file"),
                Option({make_shared<ShortForm>("-h"), make_shared<LongForm>("--help")}, {}, "help", "description every command"),
                Option({make_shared<ShortForm>("-m")}, {make_shared<IntArg>("<m>", "<int>")}, "M", "Count of M from the rules of the game"),
                Option({make_shared<ShortForm>("-n")}, {make_shared<IntArg>("<n>", "<int>")}, "N", "Count of N from the rules of the game"),
                Option({make_shared<ShortForm>("-k")}, {make_shared<IntArg>("<k>", "<int>")}, "K", "Count of K from the rules of the game")
        };

        return opts;
    }

    vector<ActualOption> parseCmd(OptionRuleSet &ruleSet, vector<string> &data) {
        vector<ActualOption> options;

        try {
            options = ruleSet.parse(data);
        }
        catch (ExactMismatch) {
            std::cerr << "Failed to parse command line arguments: use --help or -h to get help" << "\n";
            exit(EXIT_FAILURE);

        }
        catch (PartialMatch &err) {

            std::cerr << "Expected " << err.getOpt().getName();
            for (int i = 0; i < err.getOpt().getArgs().size(); ++i) {
                std::cerr << err.getOpt().getArgs()[i]->getHint();
            }
            std::cerr << ", but failed to parse." << "\n";
            exit(EXIT_FAILURE);
        }

        for (const auto &opt : options) {
            if (opt.getOpt().getName() == "help") {
                ruleSet.getHelp();
                exit(EXIT_SUCCESS);
            }
        }

        return options;
    }
}
