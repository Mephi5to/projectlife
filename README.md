Project: GameLife. 
Created by Dmitry Seleznev

Game has two modes: "Auto" and "Interactive".
AutoMode runs with console arguments. It gets arguments from console, do iterations and write result in output file.
Note: If you dont pass name of output file, it creates default file "output".

InteractiveMode runs without console arguments. It wait for your input commands and do these. 
Note: stepsCounter grow up after every commnad, besides "back" and "reset".